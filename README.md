# torpy

two simple functions which enable tor in python scripts, tested: py2.7.3  
if you want to use sophisticated tor functions, use something else, e.g. [pytorctl](https://github.com/aaronsw/pytorctl) or [stem](https://pypi.python.org/pypi/stem/).  
dependency: [socks](http://sourceforge.net/projects/socksipy/) (only one file)

**torify** *(host, port, socket_type)*  

- *host*: default `127.0.0.1`
- *port*: default `9050`
- *socket_type*: default `socks.PROXY_TYPE_SOCKS5`
- *returns*: nothing

setup of the tor socks proxy. works with everything, e.g. `urllib2` or `mechanize`

**new_identity** *(host, port, password)*  

- *host*: default `127.0.0.1`
- *port*: default `9151`
- *password*: default nothing, but use a password to secure your tor instance!
- *returns*: nothing
- sends a `SIGNAL NEWNYM` to tor using telnetlib
- you have to define the `ControlPort` in torrc (tor config file) to use this. [information here](https://www.torproject.org/docs/tor-manual.html.en)
- there is no way to know if the function succeeded, but if you've tested it once it should work, see **main** below

refresh the tor circuits. the function is blocking, because it takes some time to refresh the circuit (simply used `time.sleep(5)` to wait for that).

**main**

if you just call the script, you can call the *new_identity* function directly.  
`python torpy.py [HOST] [PORT] [PW or nothing (can contain whitespaces)]`.  
there are no default values, you have to specify every argument.
