import socks
import socket
import telnetlib, time
from os import popen3
def torify(host='127.0.0.1', port=9050, sockt=socks.PROXY_TYPE_SOCKS5):
    # tor it! <http://stackoverflow.com/a/14512227>
    def create_connection(address, timeout=None, source_address=None):
        sock = socks.socksocket()
        sock.connect(address)
        return sock
    socks.setdefaultproxy(sockt, str(host), int(port))
    # patch the socket module
    socket.socket = socks.socksocket
    socket.create_connection = create_connection

# doesn't work if the script is torify'ed
# you must call new_identity which connects
# to the local control port without using tor
def __new_identity(host='127.0.0.1', port=9151, pw=''):
    '''don't call this function directly if you use torify! update ip with ControlPort of TOR'''
    tor = telnetlib.Telnet(str(host), int(port))
    tor.write('authenticate "'+str(pw)+'"\r\n')
    tor.read_until('250 OK', 2)
    tor.write('SIGNAL NEWNYM\r\n')
    tor.read_until('250 OK', 5)
    tor.close()
    time.sleep(5)

def new_identity(host='127.0.0.1', port=9151, pw=''):
    arg=map(str, [host,port,pw])
    if all(arg[:2]):
        popen3('python '+ __file__ +' '+' '.join(arg), 'b')[1].read()
if __name__=='__main__':
    from sys import argv
    if len(argv)>=3:
        host,port,pw = argv[1],argv[2],' '.join(argv[3:])
        __new_identity(host, port, pw)
